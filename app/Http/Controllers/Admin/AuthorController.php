<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use App\User;

class AuthorController extends Controller
{
    public function index(){
      $authors = User::authors();
      return view('admin.author.index',compact('authors'));
    }

    public function destroy($id){
        $author = User::find($id);
        $delete = $author->delete();

        if($delete){
          Toastr::success('Yazar başarıyla silindi.','Başarılı');
          return redirect()->back();
        }else{
          Toastr::error('Yazar silinirken bir hata oluştu.','Hata!');
          return redirect()->back();
        }
    }
}
