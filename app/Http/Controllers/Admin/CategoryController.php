<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::latest()->get();

        return view('admin.category.index',compact('categories'));
    }


    public function create()
    {
         return view('admin.category.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,array(
            'name' => 'required|unique:categories',
            'image' => 'required|mimes:jpeg,jpg,png,gif'
        ));

        $image = $request->file('image');
        $slug  = str_slug($request->name);

        if($request->hasFile('image')){



            $currentDate = Carbon::now()->toDateString();
            $image_name = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!Storage::disk('public')->exists('category'))
            {
                Storage::disk('public')->makeDirectory('category');

            }

            $category_image = Image::make($image)->resize(1600,479)->save();
            Storage::disk('public')->put('category/'.$image_name,$category_image);

            if(!Storage::disk('public')->exists('category/slider'))
            {
                Storage::disk('public')->makeDirectory('category/slider');

            }

            $category_slider_image = Image::make($image)->resize(500,333)->save();
            Storage::disk('public')->put('category/slider/'.$image_name,$category_slider_image);

        }else
        {
            $image_name = "default.png";
        }

        $category = new Category();
        $category->name = $request->name;
        $category->slug = $slug;
        $category->image = $image_name;

        $insert = $category->save();

        if($insert){
            Toastr::success('Kayıt başarıyla eklendi','Başarılı!');
            return redirect()->route('admin.category.index');
        }else{
            Toastr::error('Kayıt eklenirken bir sorun oluştu','Hata!');
            return redirect()->route('admin.category.create');
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.category.edit',compact('category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'name' => 'required'
        ));

        $image = $request->file('image');
        $slug  = str_slug($request->name);
        $category = Category::find($id);

        if($request->hasFile('image')){



            $currentDate = Carbon::now()->toDateString();
            $image_name = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!Storage::disk('public')->exists('category'))
            {
                Storage::disk('public')->makeDirectory('category');

            }

            if(Storage::disk('public')->exists('category/'.$category->image)){
                Storage::disk('public')->delete('category/'.$category->image);
            }

            $category_image = Image::make($image)->resize(1600,479)->save();
            Storage::disk('public')->put('category/'.$image_name,$category_image);

            if(!Storage::disk('public')->exists('category/slider'))
            {
                Storage::disk('public')->makeDirectory('category/slider');

            }
            if(Storage::disk('public')->exists('category/slider/'.$category->image)){
                Storage::disk('public')->delete('category/slider/'.$category->image);
            }

            $category_slider_image = Image::make($image)->resize(500,333)->save();
            Storage::disk('public')->put('category/slider/'.$image_name,$category_slider_image);

        }else
        {
            $image_name = $category->image;
        }

        $category->name = $request->name;
        $category->slug = $slug;
        $category->image = $image_name;

        $update = $category->save();

        if($update){
            Toastr::success('Kayıt başarıyla güncellendi','Başarılı!');
            return redirect()->route('admin.category.index');
        }else{
            Toastr::error('Kayıt güncellenirken bir sorun oluştu','Hata!');
            return redirect()->route('admin.category.create');
        }
    }


    public function destroy($id)
    {
        $category = Category::find($id);

        if(Storage::disk('public')->exists('category/'.$category->image))
        {
            Storage::disk('public')->delete('category/'.$category->image);
        }

        if(Storage::disk('public')->exists('category/slider/'.$category->image))
        {
            Storage::disk('public')->delete('category/slider/'.$category->image);
        }

        $delete = $category->delete();

        if($delete) {
            Toastr::success('Kayıt silindi.', 'Başarılı!');
            return redirect()->route('admin.category.index');
        }else{
            Toastr::error('Kayıt silinirken bir sorun oluştu','Hata!');
            return redirect()->route('admin.category.index');
        }
    }
}
