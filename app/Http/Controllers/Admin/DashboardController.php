<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Post;
use App\User;
use App\Category;
use App\Tag;

class DashboardController extends Controller
{
    public function index(){
      $posts            = Post::all();
      $popular_posts    = Post::withCount('favorite_to_users')
          ->withCount('comments')
          ->orderBy('view_count','desc')
          ->orderBy('comments_count','desc')
          ->orderBy('favorite_to_users_count','desc')
          ->take(5)
          ->get();
     $author_count      = User::where('role_id',2)->count();
     $all_views         = Post::sum('view_count');
     $new_authors_today = User::where('created_at',Carbon::today())->count();
     $active_authors    = User::where('role_id',2)->withCount('comments')
          ->withCount('posts')
          ->withCount('favorite_posts')
          ->orderBy('comments_count','desc')
          ->orderBy('posts_count','desc')
          ->orderBy('favorite_posts_count','desc')
          ->take(10)
          ->get();
    $tag_count          = Tag::all()->count();
    $category_count     = Category::all()->count();
        return view('admin.dashboard', compact('posts','popular_posts','all_views','author_count','new_authors_today','active_authors','tag_count','category_count'));
    }
}
