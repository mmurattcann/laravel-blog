<?php

namespace App\Http\Controllers\Admin;

use App\Subscriber;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
class SubscriberController extends Controller
{
    public function index(){

       $subscribers = Subscriber::latest()->get();
       return view('admin.subscriber',compact('subscribers'));
    }

    public function destroy($id){
        $subscriber =  Subscriber::find($id)->delete();
        Toastr::success('Kayıt silindi.','Başarılı!');
        return redirect()->back();
    }
}
