<?php

namespace App\Http\Controllers\Admin;

use App\Tag;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{

    public function index()
    {
        $tags = Tag::all();

        return view('admin.tag.index', compact('tags'));
    }

    public function create()
    {
        return view('admin.tag.create');
    }


    public function store(Request $request)
    {
        $this->validate(request(), array(
            'name' => 'required'
        ));

        $tag = new Tag();
        $tag->name = $request->name;
        $tag->slug = str_slug($request->name);

        $insert = $tag->save();

        if ($insert) {
            Toastr::success('Kayıt başarıyla eklendi.', 'Başarılı!');
            return redirect()->route('admin.tag.index');
        } else {
            Toastr::error('Kayıt eklenirken bir sorun oluştu.', 'Hata!');
            return route()->back();
        }

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $tag = Tag::find($id);

        return view('admin.tag.edit', compact('tag'));
    }


    public function update(Request $request, $id)
    {
        $this->validate(request(), array(
            'name' => 'required'
        ));
        $tag = Tag::find($id);
        $tag->name = $request->name;
        $tag->slug = str_slug($request->name);

        $update = $tag->save();
        if ($update) {
            Toastr::success('Kayıt başarıyla güncellendi.', 'Başarılı!');
            return redirect()->route('admin.tag.index');
        } else {
            Toastr::error('Kayıt güncellenirken bir sorun oluştu.', 'Hata!');
            return route()->back();
        }

    }


    public function destroy($id)
    {
        Tag::find($id)->delete();
        Toastr::success('Kayıt Silindi.', 'Başarılı!');
        return redirect()->back();


    }
}
