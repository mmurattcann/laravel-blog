<?php

namespace App\Http\Controllers\Author;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

use App\Post;
use App\Comment;

class CommentController extends Controller
{
    public function index(){
       $posts = Auth::user()->posts;
       return view('author.comment.index', compact('posts'));

    }
    public function edit($id){
       $comment = Comment::find($id);
       return view('admin.comment.edit', compact('comment'));
    }

    public function update(Request $request, $id){
      $comment = Comment::find($id);

      $this->validate($request, array('comment' => 'required'));

      $comment->comment = $request->comment;
      $update = $comment->save();

      if($update){
        Toastr::success('Yorum Başarıyla Güncellendi','Başarılı!');
        return redirect()->route('admin.comment.index');
      }else{
        Toastr::error('Yorum Güncellenirken Bir Hata Oluştu.','Hata!');
        return redirect()->back();
      }

    }
    public function destroy($id){

        $comment = Comment::find($id);
        if ($comment->post->user->id == Auth::id())
        {
            $delete  = $comment->delete();

            if($delete){
              Toastr::success('Yorum Başarıyla Silindi.','Başarılı!');
              return redirect()->back();
            }else{
              Toastr::error('Yorum Silinirken Bir Hata Oluştu.','Hata!');
              return redirect()->back();
            }
        }
        else
        {
          Toastr::error('Bu Yorumu Silme Yetkiniz Bulunmamakta.','Hata!');
          return redirect()->back();
        }

    }
}
