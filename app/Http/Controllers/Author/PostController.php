<?php

namespace App\Http\Controllers\Author;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\NewAuthorPost;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;
use Intervention\Image\Facades\Image;
use App\Post;
use App\Category;
use App\Tag;
use App\User;
class PostController extends Controller
{
    public function index()
    {
      $posts = Auth::user()->posts()->latest()->get();
      return view('author.post.index',compact('posts'));

    }
    public function create()
    {
      $categories = Category::all();
      $tags       = Tag::all();
      return view('author.post.create',compact('categories','tags'));
    }
    public function store(Request $request)
    {

        $this->validate(request(),array(
          'title'      => 'required|unique:posts',
          'body'       => 'required',
          'image'      => 'required|mimes:jpeg,jpg,png,gif',
          'categories' => 'required',
          'tags'       => 'required'
        ));

        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if(isset($image))
        {
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug."-".$currentDate."-".uniqid().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('post'))
            {
                Storage::disk('public')->makeDirectory('post');
            }

            $postImage = Image::make($image)->resize(1600,1066)->save();
            Storage::disk('public')->put('post/'.$imageName,$postImage);
        }
        else
        {
            $imageName = "default.png";
        }

        $post = new Post();
        $post->title = $request->title;
        $post->slug  = $slug;
        $post->body  = $request->body;
        $post->user_id = Auth::id();
        $post->image  = $imageName;

        if(isset($request->status))
        {
          $post->status = true;
        }
        else
        {
          $post->status = false;
        }

        $post->is_approved = false;

        $post->save();



        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);
        // send email notification to admin
        $users = User::where('role_id',1)->get();
        \Notification::send($users, new NewAuthorPost($post));

        Toastr::success('Kayıt Başarıyla Eklendi. Yönetici onayı bekleniyor..','Başarılı!');
        return redirect()->route('author.post.index');
    }
    public function show(Post $post)
    {
      if($post->user_id != Auth::id())
        {
          Toastr::error('Bu içeriğe erişim hakkınız yok.','HATA!');
          return redirect()->back();
        }

      return view('author.post.show',compact('post'));
    }
    public function edit(Post $post)
    {
      if($post->user_id != Auth::id())
      {
          Toastr::error('Bu içeriğe erişim hakkınız yok.','HATA!');
          return redirect()->back();
      }

      $categories = Category::all();
      $tags       = Tag::all();
      return view('author.post.edit',compact('categories','tags','post'));
    }

    public function update(Request $request, Post $post)
    {
      if($post->user_id != Auth::id())
      {
          Toastr::error('Bu içeriğe erişim hakkınız yok.','HATA!');
          return redirect()->back();
      }

      $this->validate(request(), array(
          'title'      => 'required',
          'body'       => 'required',
          'categories' => 'required',
          'tags'       => 'required'
        ));

        $slug = str_slug($request->title);
        $image = $request->file('image');

        if(isset($image)){
          $this->validate(request(),array(
            'image' => 'required|mimes:jpeg,png,jpg,gif'
        ));

          if(isValid('image'))
          {
             $currentDate = Carbon::now()->toDateString();
             $imageName   = $slug."-".$currentDate."-".uniqid().".".$image->getClientOriginalExtension();

             if(!Storage::disk('public')->exists('post')){
               Storage::disk('public')->makeDirectory('post');
             }
              // delete old image if exists
            if(Storage::disk('public')->exists('post/'.$post->image)){
              Storage::disk('public')->delete('post/'.$post->image);
            }

            $postImage = Image::make($image)->resize(1600,1066)->save();
            Storage::disk('public')->put('post/'.$imageName,$postImage);
          }

        }
        else
        {
          $imageName = $post->image;
        }

        $post->user_id = Auth::id();
        $post->title   = $request->title;
        $post->body    = $request->body;
        $post->slug    = $slug;
        $post->image   = $imageName;

        if(isset($request->status))
        {
          $post->status = true;
        }
        else
        {
          $post->status = false;
        }

        $post->is_approved = false;

        $post->save();

        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);

        Toastr::success('Kayıt Başarıyla Güncellendi.','Başarılı!');
        return redirect()->route('author.post.index');
    }
    public function destroy(Post $post)
    {
        if($post->user_id != Auth::id())
        {
          Toastr::error('Bu içeriğe erişim hakkınız yok.','HATA!');
          return redirect()->back();
        }

        if(Storage::disk('public')->exists('post/'.$post->image))
        {
            Storage::disk('public')->delete('post/'.$post->image);
        }

        $post->categories()->detach();
        $post->tags()->detach();
        $post->delete();

        Toastr::success('Kayıt Silindi.','Kayıt Silindi');
        return redirect()->route('author.post.index');
    }
    public function updatePassword(){
        
    }
}
