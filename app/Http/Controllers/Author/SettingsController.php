<?php
namespace App\Http\Controllers\Author;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
class SettingsController extends Controller
{
  public function index()
  {
      return view('author.settings');
  }
  public function updateProfile(Request $request)
  {
      $this->validate($request,[
          'name' => 'required',
          'email' => 'required|email'
      ]);
      $image = $request->file('image');
      $slug = str_slug($request->name);
      $user = User::findOrFail(Auth::id());
      if (isset($image))
      {
          if($image->isValid()){

              $currentDate = Carbon::now()->toDateString();
              $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
              if (!Storage::disk('public')->exists('profile-pictures'))
              {
               Storage::disk('public')->makeDirectory('profile-pictures');
              }
    //            Delete old image form profile folder
              if (Storage::disk('public')->exists('profile-pictures/'.$user->image))
              {
                  Storage::disk('public')->delete('profile-pictures/'.$user->image);
              }
              $profile = Image::make($image)->resize(500,500)->save();
              Storage::disk('public')->put('profile-pictures/'.$imageName,$profile);
            }
        }
        else
        {
            $imageName = $user->image;
        }
      $user->name = $request->name;
      $user->email = $request->email;
      $user->image = $imageName;
      $user->about = $request->about;
      $user->save();
      Toastr::success('Profiliniz başarıyla güncellendi.','Başarılı!');
      return redirect()->back();
  }
  public function updatePassword(Request $request)
  {
      $this->validate($request,[
          'old_password' => 'required',
          'password_confirmation' => 'required|same:new_password',
      ]);
      $hashedPassword = Auth::user()->password;
      if (Hash::check($request->old_password,$hashedPassword))
      {
          if (!Hash::check($request->new_password,$hashedPassword))
          {
              $user = User::find(Auth::id());
              $user->password = Hash::make($request->new_password);
              $user->save();
              Toastr::success('Şifreniz Başarıyla Güncellendi','Başarılı');
              Auth::logout();
              return redirect()->back();
          } else {
              Toastr::error('Yeni şifreniz eskisiyle aynı olamaz.','Hata!');
              return redirect()->back();
          }
      } else {
          Toastr::error('Şifreler eşleşmiyor.','Hata!');
          return redirect()->back();
      }
  }
}
