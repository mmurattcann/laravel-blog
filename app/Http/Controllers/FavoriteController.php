<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use App\Post;

class FavoriteController extends Controller
{
    public function add($post){

      $posts      = Post::find($post);
      $user       = Auth::user();
      $isFavorite = $user->favorite_posts()->where('post_id',$post)->count();

      if($isFavorite == 0){
          $user->favorite_posts()->attach($post);
          Toastr::success('Favori İçeriklerinize Başarıyla Eklendi:'."<br/> <b>".$posts->title."</b>",'Başarılı!');
          return redirect()->back();
      }
      else{
        $user->favorite_posts()->detach($post);
        Toastr::success('Favorlerinizden Kaldırıldı:'."<br/><b>".$posts->title."</b>",'Başarılı!');
        return redirect()->back();
      }
    }
}
