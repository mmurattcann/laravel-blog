<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Post;
use App\Tag;
use App\Category;
class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->approved()->published()->paginate(6);

        return view('posts',compact('posts'));
    }
    public function postDetail($slug)
    {
        $post         = Post::where('slug',$slug)->first();
        $random_posts = Post::approved()->published()->take(3)->inRandomOrder()->get();
        $tags         = Tag::all();

        $blogKey = "blog_".$post->id;

        if (!Session::has($blogKey)) {
            $post->increment('view_count');
            Session::put($blogKey,1);
        }

        return view('post_detail',compact('post','random_posts','tags'));
    }

    public function postByCategory($slug){
       $category =  Category::where('slug',$slug)->first();
       $posts    = $category->posts()->approved()->published()->get();
       return view('category',compact('category','posts'));
    }
    public function postByTag($slug){
       $tag =  Tag::where('slug',$slug)->first();
       $posts    = $tag->posts()->approved()->published()->get();
       return view('tag',compact('tag','posts'));
    }
    public function search(Request $request){
        $query = $request->input('search_word');

        $posts = Post::where('title','like',"%".$query."%")->approved()->published()->get();
        return view('search', compact('posts','query'));
    }
}
