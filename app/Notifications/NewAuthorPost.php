<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewAuthorPost extends Notification implements ShouldQueue
{
    use Queueable;
    public $post;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($post)
    {
        $this->post = $post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Merhaba, Admin!')
                    ->subject('İçerik Yayınlama Onayı')
                    ->line($this->post->user->name.' '.'tarafından gönderilen onaylanması gereken yeni bir gönderi var.')
                    ->line('Makale Başlığı: '.$this->post->title)
                    ->line('Onaylamak için Görüntüle butonuna tıklayın')
                    ->action('Görüntüle', url(route('admin.post.show',$this->post->id)))
                    ->line('İlginiz için teşekkürler!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
