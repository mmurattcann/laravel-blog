<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewPostNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $post;
    public function __construct($post)
    {
        $this->post = $post;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Merhaba, sevgili kullanıcımız.')
                    ->subject('Yeni Makalemiz Yayında!')
                    ->line($this->post->title.' '.'başlıklı yeni makalemiz yayınlandı.')
                    ->line('Okumak için lütfen Görüntüle butonuna tıklayın.')
                    ->action('Görüntüle', url(route('anasayfa')))
                    ->line('İlginiz İçin Teşekkürler!');
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
