<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'MD.Admin',
            'username' => 'admin',
            'email'    => 'admin@blog.com',
            'password' => bcrypt('rootadmin'),
            'role_id'  => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'MD.Author',
            'username' => 'author',
            'email'    => 'author@blog.com',
            'password' => bcrypt('rootauthor'),
            'role_id'  => 1,
        ]);
    }
}
