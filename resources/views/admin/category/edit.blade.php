@extends('layouts.backend.app')

@section('title','Kategori Düzenle')

@push('css')

@endpush

@section('content')
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                         Kategori Düzenle

                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <form action = "{{ route('admin.category.update', $category->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label>Yüklü Görsel</label>
                            <img style="width:200px; height: 150px;"
                                 src="{{asset('category/'.$category->image)}}"
                                 alt="{{$category->name}}">
                        </div>
                        <div class="form-group">
                            <label> Yeni Resim Yükle</label>
                            <input type="file" name="image">
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="name" name="name" class="form-control" value="{{$category->name}}">
                                <label class="form-label">Etiket Adı</label>

                            </div>
                            @if($errors->has('name'))

                                {!! $errors->first('name', '<p class="">:message</p>') !!}
                            @endif
                        </div>

                        <br>
                        <a href="{{ route('admin.category.index') }}" class="btn btn-danger m-t-15 waves-effect">İptal</a>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Kaydet</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
@endsection

@push('js')

@endpush

