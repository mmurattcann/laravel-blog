@extends('layouts.backend.app')

@section('title')
  {{ Auth::user()->name }} | Anasayfa
@endsection

@push('css')

@endpush

@section('content')
    <div class="container-fluid">
        <div class="block-header">
              <h2>Profil İstatistikleri</h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">Toplam İçerik</div>
                        <div class="number count-to" data-from="0" data-to="{{$posts->count()}}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">favorite_border</i>
                    </div>
                    <div class="content">
                        <div class="text">Favorileriniz</div>
                        <div class="number count-to" data-from="0" data-to="{{Auth::user()->favorite_posts->count()}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">visibility</i>
                    </div>
                    <div class="content">
                        <div class="text">Görüntülenme</div>
                        <div class="number count-to" data-from="0" data-to="{{ $all_views }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="content">
                        <div class="text">Kullanıcılar</div>
                        <div class="number count-to" data-from="0" data-to="{{$author_count}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-amber hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">view_module</i>
                    </div>
                    <div class="content">
                        <div class="text">Kategoriler</div>
                        <div class="number count-to" data-from="0" data-to="{{$category_count}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-blue-grey hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">label</i>
                    </div>
                    <div class="content">
                        <div class="text">Etiketler</div>
                        <div class="number count-to" data-from="0" data-to="{{$tag_count}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->

        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>En Popüler 5 İçerik</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Başlık</th>
                                    <th>Yazar</th>
                                    <th>Görüntülenme</th>
                                    <th>Favroriler</th>
                                    <th>Yorumlar</th>
                                    <th>Durum</th>
                                    <th>İncele</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($popular_posts as $key => $populars)


                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $populars->title }}</td>
                                    <td>{{ $populars->user->name }}</td>
                                    <td>{{ $populars->view_count }}</td>
                                    <td>{{ $populars->favorite_to_users_count }}</td>
                                    <td>{{ $populars->comments_count}}</td>
                                    <td>
                                        @if ($populars->status == true)
                                            <span class="label bg-green">Yayında</span>
                                        @else
                                           <span class="label bg-pink">Pasif</span>
                                        @endif
                                    </td>
                                    <td><a href="{{ route('post.detail',$populars->slug)}}" class="btn btn-info">Görüntüle</a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Task Info -->

        </div>
        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>Başlıca Yazarlar</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Adı</th>
                                    <th>Yazar</th>
                                    <th>İçerik Sayısı</th>
                                    <th>Favrori İçerikler</th>
                                    <th>Yorumlar</th>
                                    <th>İncele</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($active_authors as $key => $author)


                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $author->title }}</td>
                                    <td>{{ $author->name }}</td>
                                    <td>{{ $author->posts->count() }}</td>
                                    <td>{{ $author->favorite_posts_count }}</td>
                                    <td>{{ $author->comments_count}}</td>
                                    <td><a href="{{ route('author.profile',$author->username)}}" class="btn btn-info">Görüntüle</a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Task Info -->

        </div>
    </div>
@endsection

@push('js')
  <!-- Jquery CountTo Plugin Js -->
  <script src="{{asset('assets/backend/plugins/jquery-countto/jquery.countTo.js')}}"></script>

  <script src="{{asset('assets/backend/js/pages/index.js')}}"></script>

  <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
@endpush
