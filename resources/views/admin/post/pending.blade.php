@extends('layouts.backend.app')

@section('title','İçerikler')


@push('css')

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <a  class="btn btn-primary btn-lg waves-effect text-center" href="{{ route('admin.post.create')}}">
                <i class="material-icons" >add</i>Yeni Kayıt
            </a>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Onay Bekleyen İçerikler
                            @if($posts->count() > 0)
                                <span class="bg-pink" style="padding: 5px">{{$posts->count()}}</span>
                            @endif
                        </h2>
                    </div>
                    <div class="body">
                    @if($posts->count() > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Görsel</th>
                                        <th>Başlık</th>
                                        <th><i class="material-icons">visibility</i></th>
                                        <th>Onaylı mı?</th>
                                        <th>Durumu</th>
                                        <th>Oluşturulma Tarihi</th>
                                        <th  class="text-center">İşlemler</th>

                                    </tr>
                                </thead>

                                <tbody>
                                @foreach($posts as $i => $post)
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td>
                                            <img style="width:150px; height: 150px;"
                                                src="{{url('storage/post/'.$post->image)}}"
                                                alt="{{asset($post->title)}}">
                                        </td>
                                        <td>{{ $post->title }}</td>
                                        <td>{{$post->view_count}}</td>
                                        <td>
                                            @if($post->is_approved == true)
                                                <span class="badge bg-blue">Onaylı</span>
                                            @else
                                                <span class="badge bg-pink">Onay Bekliyor</span>
                                            @endif

                                        </td>
                                        <td>
                                            @if($post->status == true)
                                                <span class="badge bg-blue">Yayında</span>
                                            @else
                                                <span class="badge bg-pink">Onay Bekliyor</span>
                                            @endif
                                        </td>
                                        <td>{{$post->created_at}}</td>
                                        <td class="text-center">
                                            <a class="btn btn-success btn-block" href="{{route('admin.post.edit',$post->id)}}" >Düzenle</a>
                                            <a class="btn btn-warning success btn-block" href="{{ route('admin.post.show',$post->id) }}"><i class="material-icons">visibility</i></a>
                                            <button  class="btn btn-danger btn-block" type="button" onclick="deletePost({{ $post->id  }})">
                                                <i class="material-icons">delete</i>

                                            </button>
                                            <form action="{{ route('admin.post.destroy',$post->id)}}" method="POST" id="delete-form-{{$post->id}}" style="display:none">
                                                @csrf
                                                @method('DELETE')


                                            </form>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    @else
                            <div class="alert alert-warning">
                                Onaylanmayı bekleyen herhangi bir içerik bulunamadı. 
                            </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
        function deletePost(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false,
            })

            swalWithBootstrapButtons.fire({
                title: 'Silmek istediğinize emin misiniz?',
                text: "Bilgiler kalıcı olarak silinecek!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Evet, sil!',
                cancelButtonText: 'Vazgeç!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                   event.preventDefault();
                   document.getElementById('delete-form-'+id).submit();

                }
            })
        }
    </script>
@endpush
