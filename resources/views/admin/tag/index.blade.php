@extends('layouts.backend.app')

@section('title','Etiketler')


@push('css')

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <a  class="btn btn-primary btn-lg waves-effect text-center" href="{{ route('admin.tag.create')}}">
                <i class="material-icons" >add</i>Yeni Kayıt
            </a>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Kategoriler
                            @if($tags->count() > 0)
                                <span class="bg-pink" style="padding: 5px">{{$tags->count()}}</span>
                            @endif
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                    @if($tags->count() > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Etiket Adı</th>
                                        <th>Slug</th>
                                        <th>İçerik Sayısı</th>
                                        <th  class="text-center">İşlemler</th>

                                    </tr>
                                </thead>

                                <tbody>
                                @foreach($tags as $i => $tag)
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td>{{ $tag->name }}</td>
                                        <td>{{$tag->slug}}</td>
                                        <td>{{$tag->posts->count()}}</td>
                                        <td class="text-center">
                                            <a class="btn btn-success " href="{{route('admin.tag.edit',$tag->id)}}" >Düzenle</a>
                                            <button  class="btn btn-danger" type="button" onclick="deleteTag({{ $tag->id  }})">
                                                <i class="material-icons">delete</i>

                                            </button>
                                            <form action="{{ route('admin.tag.destroy',$tag->id)}}" method="POST" id="delete-form-{{$tag->id}}" style="display:none">
                                                @csrf
                                                @method('DELETE')


                                            </form>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    @else
                            <div class="alert alert-warning">
                               Hiçbir kayıt bulunamadı. Yeni kayıt eklemek için  <strong><a href="{{ route('admin.tag.create') }}">buraya</a></strong> tıklayın.
                            </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>
        function deleteTag(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false,
            })

            swalWithBootstrapButtons.fire({
                title: 'Silmek istediğinize emin misiniz?',
                text: "Bilgiler kalıcı olarak silinecek!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Evet, sil!',
                cancelButtonText: 'Vazgeç!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                   event.preventDefault();
                   document.getElementById('delete-form-'+id).submit();

                }
            })
        }
    </script>
@endpush

