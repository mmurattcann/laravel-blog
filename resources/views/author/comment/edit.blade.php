@extends('layouts.backend.app')

@section('title','Yorum Düzenle')

@push('css')

@endpush

@section('content')
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                         Yorum Düzenle

                    </h2>
                </div>
                <div class="body">
                    <form action = "{{ route('admin.comment.update', $comment->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea id="tinymce" name="comment" rows="5">{{ $comment->comment }}</textarea>
                            </div>
                            @if($errors->has('name'))

                                {!! $errors->first('name', '<p class="">:message</p>') !!}
                            @endif
                        </div>

                        <br>
                        <a href="{{ route('admin.comment.index') }}" class="btn btn-danger m-t-15 waves-effect">İptal</a>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Kaydet</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
@endsection

@push('js')
  <!-- TinyMCE -->
  <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
  <script>
      $(function () {
          //TinyMCE
          tinymce.init({
              selector: "textarea#tinymce",
              theme: "modern",
              height: 300,
              plugins: [
                  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                  'searchreplace wordcount visualblocks visualchars code fullscreen',
                  'insertdatetime media nonbreaking save table contextmenu directionality',
                  'emoticons template paste textcolor colorpicker textpattern imagetools'
              ],
              toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
              toolbar2: 'print preview media | forecolor backcolor emoticons',
              image_advtab: true
          });
          tinymce.suffix = ".min";
          tinyMCE.baseURL = '{{asset('assets/backend/plugins/tinymce')}}';
      });
  </script>
@endpush
