@extends('layouts.backend.app')

@section('title','Yorumlar')

@push('css')

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Yorumlar

                        </h2>
                    </div>
                    <div class="body">
                    {{-- @if($posts->comments->count() > 0) --}}
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Ait Olduğu İçerik</th>
                                        <th>Yorum</th>
                                        <th>Oluşturulma Tarihi</th>
                                        <th class="text-center">İşlemler</th>

                                    </tr>
                                </thead>

                                <tbody>
                                @foreach($posts as $post)
                                  @foreach ($post->comments as $i => $comment)


                                    <tr>
                                        <td>{{$i+1}}</td>

                                        <td><a href="{{ route('post.detail',$comment->post->slug) }}">{{ $comment->post->title}}</a></td>
                                        <td>{!! str_limit($comment->comment,60,$end = "...") !!}</td>
                                        <td>{!! date('d/m/Y', strtotime($comment->created_at)) !!}</td>
                                        <td class="text-center">
                                            <a class="btn btn-success " href="{{route('author.comment.edit',$comment->id)}}" >Düzenle</a>
                                            <button  class="btn btn-danger" type="button" onclick="deleteComment({{ $comment->id  }})">
                                                <i class="material-icons">delete</i>

                                            </button>
                                            <form action="{{ route('author.comment.destroy',$comment->id)}}" method="POST" id="delete-form-{{$comment->id}}" style="display:none">
                                                @csrf
                                                @method('DELETE')
                                            </form>

                                        </td>
                                    </tr>
                                    @endforeach
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    {{-- @else
                            <div class="alert alert-warning">
                               Henüz gönderilerinize yapılan herhangi bir yorum bulunmamakta.
                            </div>
                    @endif --}}
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>
        function deleteComment(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false,
            })

            swalWithBootstrapButtons.fire({
                title: 'Silmek istediğinize emin misiniz?',
                text: "Bilgiler kalıcı olarak silinecek!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Evet, sil!',
                cancelButtonText: 'Vazgeç!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                   event.preventDefault();
                   document.getElementById('delete-form-'+id).submit();

                }
            })
        }
    </script>
@endpush
