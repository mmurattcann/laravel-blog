@extends('layouts.backend.app')

@section('title')
  {{ Auth::user()->name }} | Anasayfa
@endsection

@push('css')

@endpush

@section('content')
    <div class="container-fluid">
        <div class="block-header">
              <h2>Profil İstatistikleri</h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">Toplam İçerik</div>
                        <div class="number count-to" data-from="0" data-to="{{$posts->count()}}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">alarm</i>
                    </div>
                    <div class="content">
                        <div class="text">Bekleyen İçerikler</div>
                        <div class="number count-to" data-from="0" data-to="{{$total_pending_posts}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">forum</i>
                    </div>
                    <div class="content">
                        <div class="text">Favoriler</div>
                        <div class="number count-to" data-from="0" data-to="{{Auth::user()->favorite_posts->count()}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">visibility</i>
                    </div>
                    <div class="content">
                        <div class="text">Görüntülenme</div>
                        <div class="number count-to" data-from="0" data-to="{{ $total_views }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->

        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>En Popüler 5 İçerik</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Başlık</th>
                                    <th>Görüntülenme</th>
                                    <th>Favroriler</th>
                                    <th>Yorumlar</th>
                                    <th>Durum</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($popular_posts as $key => $populars)


                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $populars->title }}</td>
                                    <td>{{ $populars->view_count }}</td>
                                    <td>{{ $populars->favorite_to_users_count }}</td>
                                    <td>{{ $populars->comments_count}}</td>
                                    <td>
                                        @if ($populars->status == true)
                                            <span class="label bg-green">Yayında</span>
                                        @else
                                           <span class="label bg-pink">Pasif</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Task Info -->

        </div>
    </div>
@endsection

@push('js')
  <!-- Jquery CountTo Plugin Js -->
  <script src="{{asset('assets/backend/plugins/jquery-countto/jquery.countTo.js')}}"></script>

  <script src="{{asset('assets/backend/js/pages/index.js')}}"></script>

  <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
@endpush
