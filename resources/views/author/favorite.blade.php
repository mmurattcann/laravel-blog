@extends('layouts.backend.app')

@section('title','İçerikler')


@push('css')

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Favori İçerikler
                            @if($posts->count() > 0)
                                <span class="bg-pink" style="padding: 5px">{{$posts->count()}}</span>
                            @endif
                        </h2>
                    </div>
                    <div class="body">
                    @if($posts->count() > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Görsel</th>
                                        <th>Başlık</th>
                                        <th><i class="material-icons">visibility</i></th>
                                        <th><i class="material-icons">favorite</i></th>
                                        <th><i class="material-icons">comment</i>

                                        <th  class="text-center">İşlemler</th>

                                    </tr>
                                </thead>

                                <tbody>
                                @foreach($posts as $i => $post)
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td>
                                            <img style="width:150px; height: 150px;"
                                                src="{{url('storage/post/'.$post->image)}}"
                                                alt="{{asset($post->title)}}">
                                        </td>
                                        <td>{{ $post->title }}</td>
                                        <td>{{$post->view_count}}</td>
                                        <td>
                                              {{ $post->favorite_to_users->count() <= 0 ? "0":$post->favorite_to_users->count()}}
                                        </td>
                                        <td>
                                              0
                                        </td>
                                        <td class="text-center">

                                            <a class="btn btn-warning success btn-block" href="{{ route('author.post.show',$post->id) }}"><i class="material-icons">visibility</i></a>
                                            <button  class="btn btn-danger btn-block" type="button" onclick="removePost({{ $post->id  }})">
                                                <i class="material-icons">delete</i>

                                            </button>
                                            <form action="{{ route('post.favorite',$post->id)}}" method="POST" id="remove-form-{{$post->id}}" style="display:none">
                                                @csrf
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    @else
                            <div class="alert alert-warning">
                                Favorilerinizde herhangi bir içerik bulunamadı.
                            </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
        function removePost(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false,
            })

            swalWithBootstrapButtons.fire({
                title: 'Favorilerinizden kaldırmak istediğinize emin misiniz?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Evet, kaldır!',
                cancelButtonText: 'Vazgeç!',
                reverseButtons: false
            }).then((result) => {
                if (result.value) {
                   event.preventDefault();
                   document.getElementById('remove-form-'+id).submit();

                }
            })
        }
    </script>
@endpush
