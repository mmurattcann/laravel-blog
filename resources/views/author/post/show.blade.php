@extends('layouts.backend.app')

@section('title','Detay' )

@push('css')
    <!-- Bootstrap Select Css -->
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" /
@endpush

@section('content')
    <div class="container-fluid">
    <!-- Vertical Layout | With Floating Label -->

        <a href="{{ route('author.post.index')}}" class="btn btn-danger btn-lg">Geri</a><br><br>
        <div class="row clearfix">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

                <div class="card">
                    <div class="header">
                        <h2>
                            {{ $post->title}}
                            <small>
                                Yazan:{{ $post->user->name }}<br>
                                Tarih:{{ $post->created_at->toFormattedDateString() }}
                            </small>
                        </h2>

                    </div>
                    <div class="body">

                        {!! $post->body !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="card">
                      <div class="header bg-orange">
                          <h2>
                             İçeriğe Ait Görsel

                          </h2>

                      </div>
                      <div class="body">

                              @csrf
                              <div class="form-group form-float">
                                  <div class="form-line ">

                                    <img style="padding:0px;margin:0px;width:270px; height:300" src="{{ url('storage/post/'.$post->image) }}" alt="{{ $post->title }}">
                                  </div>
                              </div>
                      </div>
                  </div>
                <div class="card">
                    <div class="header bg-cyan">
                        <h2>
                            Kategoriler

                        </h2>

                    </div>
                    <div class="body">

                            @csrf
                            <div class="form-group form-float">
                                <div class="form-line {{$errors->has('categories')?'focused error':''}}">

                                    @foreach($post->categories as $category)
                                        <span class="bg-cyan" style="padding:5px;">{{$category->name}}</span>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                    </div>
                </div>

            <div class="card">
                    <div class="header bg-orange">
                        <h2>
                           Etiketler

                        </h2>

                    </div>
                    <div class="body">

                            @csrf
                            <div class="form-group form-float">
                                <div class="form-line {{$errors->has('tags')?'focused error':''}}">

                                    @foreach($post->tags as $tag)
                                        <span class="bg-orange" style="padding:5px;">{{$tag->name}}</span>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                    </div>
                </div>

                </div>
        </div>
    <!-- Vertical Layout | With Floating Label -->
    </div>
@endsection

@push('js')
    <!-- Select Plugin Js -->
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <!-- TinyMCE -->
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <script>
        $(function () {
            //TinyMCE
            tinymce.init({
                selector: "textarea#tinymce",
                theme: "modern",
                height: 300,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons',
                image_advtab: true
            });
            tinymce.suffix = ".min";
            tinyMCE.baseURL = '{{asset('assets/backend/plugins/tinymce')}}';
        });
    </script>
@endpush
