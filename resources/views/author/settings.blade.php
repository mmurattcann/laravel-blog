@extends('layouts.backend.app')

@section('title','Ayarlar')

@push('css')

@endpush

@section('content')
  <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Profil
                            </h2>

                              @if($errors->any())
                                <div class="alert bg-pink">
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach

                              </div>
                              @endif
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#profile_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">face</i> Profil
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#change_password_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">vpn_key</i> Şifre
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade in active" id="profile_with_icon_title">

                                <form class="form-horizontal" method="POST" action="{{ route('author.update.profile') }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="name" style="margin-top:75px">Profil Resmi</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                    <img src="{{ url('storage/profile-pictures/'.Auth::user()->image )}}" alt="{{ Auth::user()->name }}"
                                                         style="width:150px; height:150px; border-radius:50%;">
                                            </div>
                                        </div>
                                    </div><br><br>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="">Profil Resmini Değiştir</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                      <input type="file" name="image" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="name">Ad Soyad</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="name" name="name" class="form-control" placeholder="Örn: John DOE"
                                                           value="{{ Auth::user()->name }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email_address_2">Email Address</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="email_address_2" class="form-control" name="email" placeholder="Örn: johndoe@blog.com"
                                                           value="{{ Auth::user()->email }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="name">Hakkımda</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <textarea type="text" id="tinymce" name="about" class="form-control" placeholder="Örn: John DOE">
                                                           {{ Auth::user()->about }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Güncelle</button>
                                        </div>
                                    </div>
                                </form>

                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="change_password_with_icon_title">
                                  <form class="form-horizontal" method="POST" action="{{ route('author.update.password') }}">
                                      @csrf
                                      @method('PUT')

                                      <div class="row clearfix">
                                          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                              <label for="old_password">Mevcut Şifre</label>
                                          </div>
                                          <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                              <div class="form-group">
                                                  <div class="form-line">
                                                      <input type="password" id="old_password" name="old_password" class="form-control">
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row clearfix">
                                          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                              <label for="new_password">Yeni Şifre</label>
                                          </div>
                                          <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                              <div class="form-group">
                                                  <div class="form-line">
                                                      <input type="password" id="new_password" name="new_password" class="form-control">
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row clearfix">
                                          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                              <label for="password_confirmation">Yeni Şifre (Tekrar)</label>
                                          </div>
                                          <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                              <div class="form-group">
                                                  <div class="form-line">
                                                      <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row clearfix">
                                          <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                              <button type="submit" class="btn btn-primary m-t-15 waves-effect">Güncelle</button>
                                          </div>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@push('js')
  <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
  <script>
      $(function () {
          //TinyMCE
          tinymce.init({
              selector: "textarea#tinymce",
              theme: "modern",
              height: 300,
              plugins: [
                  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                  'searchreplace wordcount visualblocks visualchars code fullscreen',
                  'insertdatetime media nonbreaking save table contextmenu directionality',
                  'emoticons template paste textcolor colorpicker textpattern imagetools'
              ],
              toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
              toolbar2: 'print preview media | forecolor backcolor emoticons',
              image_advtab: true
          });
          tinymce.suffix = ".min";
          tinyMCE.baseURL = '{{asset('assets/backend/plugins/tinymce')}}';
      });
  </script>
@endpush
