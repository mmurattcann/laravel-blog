<header>
    <div class="container-fluid position-relative no-side-padding">

        <a href="#" class="logo">LOGO</a>

        <div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="ion-navicon"></i></div>

        <ul class="main-menu visible-on-click" id="main-menu">
            <li><a href="{{ route('anasayfa') }}">Anasayfa</a></li>
            <li><a href="{{ route('post.index') }}">Makaleler</a></li>
            @guest
             <li><a href="{{ route('login') }}"></a></li>
            @else
              @if (Auth::user()->role_id == 1)
                 <li><a href="{{ route('admin.dashboard') }}">Yönetim Paneli</a></li>
              @endif
              @if (Auth::user()->role_id == 2)
                <li><a href="{{ route('author.dashboard') }}">Yönetim Paneli</a></li>
              @endif
            @endguest
        </ul><!-- main-menu -->

        <div class="src-area">
            <form action="{{ route('search') }}" method="GET">
                <button class="src-btn" type="submit"><i class="ion-ios-search-strong"></i></button>
                <input class="src-input" type="text" name="search_word" placeholder="Aranacak anahtar kelimeyi yazın..">
            </form>
        </div>

    </div><!-- conatiner -->
</header>
