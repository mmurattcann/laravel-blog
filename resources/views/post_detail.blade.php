@extends('layouts.frontend.app')

@section('title')
  {{ $post->title }}
@endsection

@push('css')

  <link href="{{ asset('assets/frontend/css/single-post/styles.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/frontend/css/single-post/responsive.css') }} " rel="stylesheet">

  <style>
      .header-bg{
        height:480px;
        width:100%;
        background-image: url({{ url('storage/post/'.$post->image) }});
        background-size: cover;
        background-position: center;
      }
  </style>
@endpush

@section('content')

	<div class="header-bg">

	</div>
<section class="post-area section">
		<div class="container">

			<div class="row">

				<div class="col-lg-8 col-md-12 no-right-padding">

					<div class="main-post">

						<div class="blog-post-inner">

              <div class="post-info">

								<div class="left-area">
									<a class="avatar" href="#"><img src="{{ url('storage/profile-pictures/'.$post->user->image) }}" style="width:58px; height:58px;" alt="{{ $post->user->name }}"></a>
								</div>

								<div class="middle-area" style="">
									<a class="name" href="#"><b>{{$post->user->name}}</b></a>
									<h6 class="date">{{ $post->created_at->diffForHumans() }}</h6>
								</div>

							</div><!-- post-info -->



							{{-- <div class="post-image"><img src="{{ url('storage/post/'.$post->image) }}" alt="Blog Image"></div> --}}


              	<h3 class="title text-center"><b>{{ $post->title }}</b></h3>
							<p class="para"> {!! html_entity_decode($post->body) !!}</p>

							<ul class="tags">
                @foreach ($post->tags as $tag)
                    <li><a href="{{ route('tag.posts',$tag->slug) }}">{{ $tag->name }}</a></li>
                @endforeach
							</ul>
						</div><!-- blog-post-inner -->

						<div class="post-icons-area">
							<ul class="post-icons">
                <li>

                      @guest
                         <a href="javascript:void(0);"><i class="ion-heart" onclick="toastr.warning('Favorilerinize Eklemek İçin Giriş  Yapmanız Gerekiyor...','Uyarı',{
                               closeButton: true,
                               progressBar: true,
                           })">
                         </i>{{ $post->favorite_to_users->count()}}
                        </a>
                      @else
                        <a href="javascript:void(0);"><i class="ion-heart" onclick="document.getElementById('favorite-form-{{ $post->id }}').submit();"
                             class="{{ !Auth::user()->favorite_posts()->where('post_id',$post->id)->count() == 0 ? "favorite-posts":"" }}">
                           </i>{{ $post->favorite_to_users->count()}}
                        </a>
                        <form id="favorite-form-{{ $post->id }}" action="{{ route('post.favorite',$post->id) }}" method="POST" style="display:none">
                          @csrf
                        </form>
                      @endguest
                    </a>
                </li>
                <li><a href="#"><i class="ion-chatbubble"></i>{{ 5 }}</a></li>
                <li><i class="ion-eye"></i>{{ $post->view_count}}</li>
							</ul>

							<ul class="icons">
								<li>SHARE : </li>
								<li><a href="#"><i class="ion-social-facebook"></i></a></li>
								<li><a href="#"><i class="ion-social-twitter"></i></a></li>
								<li><a href="#"><i class="ion-social-pinterest"></i></a></li>
							</ul>
						</div>


					</div><!-- main-post -->
				</div><!-- col-lg-8 col-md-12 -->

				<div class="col-lg-4 col-md-12 no-left-padding">

					<div class="single-post info-area">

						<div class="sidebar-area about-area">
							<h4 class="title"><b>Yazar Hakkında</b></h4>
							<p>{!! $post->user->about!!}</p>
						</div>

						<div class="sidebar-area subscribe-area">

							<h4 class="title"><b>SUBSCRIBE</b></h4>
							<div class="input-area">
								<form>
									<input class="email-input" type="text" placeholder="Enter your email">
									<button class="submit-btn" type="submit"><i class="icon ion-ios-email-outline"></i></button>
								</form>
							</div>

						</div><!-- subscribe-area -->

						<div class="tag-area">

							<h4 class="title"><b>Etiket Bulutu</b></h4>
							<ul>
                @foreach ($tags as $tag)
                    <li><a href="{{ route('tag.posts',$tag->slug) }}">{{ $tag->name }}</a></li>
                @endforeach

							</ul>

						</div><!-- subscribe-area -->

					</div><!-- info-area -->

				</div><!-- col-lg-4 col-md-12 -->

			</div><!-- row -->

		</div><!-- container -->

</section>
	<section class="recomended-area section">
		<div class="container">
			<div class="row">

    @foreach ($random_posts as $random_post)
				<div class="col-lg-4 col-md-6">
					<div class="card h-100">
						<div class="single-post post-style-1">

							<div class="blog-image"><img src="{{ url('storage/post/'.$random_post->image) }}" alt="Blog Image"></div>

							<a class="avatar" href="{{ route('author.profile',$random_post->user->username) }}"><img src="{{ url('storage/profile-pictures/'.$random_post->user->image) }}" alt="{{ $random_post->user->name }}"></a>

							<div class="blog-info">

								<h4 class="title"><a href="{{ route('post.detail',$random_post->slug) }}"><b>{{ $random_post->title }}</b></a></h4>

								<ul class="post-footer">
									<li>

                        @guest
                           <a href="javascript:void(0);"><i class="ion-heart" onclick="toastr.warning('Favorilerinize Eklemek İçin Giriş  Yapmanız Gerekiyor...','Uyarı',{
                                 closeButton: true,
                                 progressBar: true,
                             })">
                           </i>{{ $random_post->favorite_to_users->count()}}
                          </a>
                        @else
                          <a href="javascript:void(0);"><i class="ion-heart" onclick="document.getElementById('favorite-form-{{ $post->id }}').submit();"
                               class="{{ !Auth::user()->favorite_posts()->where('post_id',$random_post->id)->count() == 0 ? "favorite-posts":"" }}">
                             </i>{{ $random_post->favorite_to_users->count()}}
                          </a>
                          <form id="favorite-form-{{ $post->id }}" action="{{ route('post.favorite',$random_post->id) }}" method="POST" style="display:none">
                            @csrf
                          </form>
                        @endguest
                      </a>
                  </li>
									<li><i class="ion-chatbubble"></i>{{ $random_post->comments->count() }}</li>
									<li><a href="#"><i class="ion-eye"></i>{{ $random_post->view_count}}</a></li>
								</ul>

							</div><!-- blog-info -->
						</div><!-- single-post -->
					</div><!-- card -->
				</div><!-- col-md-6 col-sm-12 -->
   @endforeach

			</div><!-- row -->

		</div><!-- container -->
	</section>

	<section class="comment-section">
		<div class="container">
			<h4><b>Yorum Yap</b></h4>
			<div class="row">

				<div class="col-lg-8 col-md-12">
					<div class="comment-form">
            @guest
              <p>Yorum yapabilmek için lütfen giriş yapın.</p>
              <a href="{{ route('login') }}" class="btn btn-info"> Giriş Yap</a>
            @else
  						<form method="post" action="{{ route('comment.store',$post->id) }}">
                @csrf
  							<div class="row">

  								<div class="col-sm-12">
  									<textarea name="comment" rows="2" class="text-area-messge form-control"
  										placeholder="Yorumunuz..." aria-required="true" aria-invalid="false"></textarea >
  								</div><!-- col-sm-12 -->
  								<div class="col-sm-12">
  									<button class="submit-btn" type="submit" id="form-submit"><b>Gönder</b></button>
  								</div><!-- col-sm-12 -->

  							</div><!-- row -->
  						</form>
            @endguest
					</div><!-- comment-form -->

					<h4><b>YORUMLAR({{ $post->comments->count() }})</b></h4>
          <div class="commnets-area">
            @if ($post->comments->count() > 0)
              @foreach ($post->comments as $comment)
                    <div class="comment">
                      <div class="post-info">
                        <div class="left-area">
                          <a class="avatar" href="#"><img src="{{ url('storage/profile-pictures/'.$comment->user->image) }}" alt="{{ $comment->user->name }}"></a>
                        </div>

                        <div class="middle-area">
                          <a class="name" href="#"><b>{{ $comment->user->name }}</b></a>
                          <h6 class="date">{{ $comment->created_at->diffForHumans() }}</h6>
                        </div>

                        <div class="right-area">
                          <h5 class="reply-btn" ><a href="#"><b>REPLY</b></a></h5>
                        </div>

                      </div><!-- post-info -->

                      <p>{{ $comment->comment}}</p>

                    </div>
              @endforeach
            @else
                <p>İlk Yorum Yapan Siz Olun.</p>
            @endif

					</div><!-- commnets-area -->
				</div><!-- col-lg-8 col-md-12 -->

			</div><!-- row -->

		</div><!-- container -->
	</section>


@endsection

@push('js')

@endpush
