@extends('layouts.frontend.app')
@section('title')
  {{ $author->name }}
@endsection


@push('css')


    <link href="{{asset('assets/frontend/css/category-sidebar/styles.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/category-sidebar/responsive.css') }}" rel="stylesheet">
    <style>
      .favorite-posts{
        color:red ;
      }
    </style>
@endpush

@section('content')
  <div class="slider display-table center-text">
		<h1 class="title display-table-cell"><b>{{ $author->name}}</b></h1>
	</div><!-- slider -->

	<section class="blog-area section">
		<div class="container">

			<div class="row">

				<div class="col-lg-8 col-md-12">
					<div class="row">
    @if($posts->count() > 0)
        @foreach ($posts as $author_posts)

						<div class="col-md-6 col-sm-12">
							<div class="card h-100">
								<div class="single-post post-style-1">

									<div class="blog-image"><img src="{{ url('storage/post/'.$author_posts->image) }}" alt="{{$author_posts->title}}"></div>

									<a class="avatar" href="#"><img src="{{ url('storage/profile-pictures/'.$author->image) }}" alt="{{ $author->name}}"></a>

									<div class="blog-info">

										<h4 class="title"><a href="{{ route('post.detail',$author_posts->slug) }}"><b>{{ $author_posts->title }}</b></a></h4>

										<ul class="post-footer">
                      <li>
                        @guest
                           <a href="javascript:void(0);"><i class="ion-heart" onclick="toastr.warning('Favorilerinize Eklemek İçin Giriş  Yapmanız Gerekiyor...','Uyarı',{
                               closeButton: true,
                               progressBar: true,
                           })">
                                </i>{{ $author_posts->favorite_to_users->count()}}
                           </a>
                        @else
                          <a href="javascript:void(0);"><i class="ion-heart" onclick="document.getElementById('favorite-form-{{ $author_posts->id }}').submit();"
                               class="{{ !Auth::user()->favorite_posts()->where('post_id',$author_posts->id)->count() == 0 ? "favorite-posts":"" }}">
                               </i>{{ $author_posts->favorite_to_users->count()}}
                          </a>
                          <form id="favorite-form-{{ $author_posts->id }}" action="{{ route('post.favorite',$author_posts->id) }}" method="POST" style="display:none">
                            @csrf
                          </form>
                        @endguest

                      </li>
											<li><a href="#"><i class="ion-chatbubble"></i>{{ $author_posts->comments->count() }}</a></li>
											<li><a href="#"><i class="ion-eye"></i>{{ $author_posts->view_count }}</a></li>
										</ul>

									</div><!-- blog-info -->
								</div><!-- single-post -->
							</div><!-- card -->
						</div><!-- col-md-6 col-sm-12 -->
       @endforeach
   @else
      <h6>Yazara ait herhangi bir içerik bulunamadı</h6> <br>
      	<a class="bg-info" href="{{route('anasayfa')}}"><b>Anasayfa</b></a>
   @endif

	   </div><!-- row -->

				</div><!-- col-lg-8 col-md-12 -->

				<div class="col-lg-4 col-md-12 ">

					<div class="single-post info-area ">

						<div class="about-area">
							<h4 class="title"><b>{{ $author->name }}</b></h4>
							<p>{!! $author->about !!}</p>
              <strong>Toplam İçerik Sayısı: </strong> {{ $posts->count() }} <br>
                <strong>Toplam Yorum Sayısı: </strong> {{ $author->comments->count() }}
						</div>

						<div class="subscribe-area">

							<h4 class="title"><b>SUBSCRIBE</b></h4>
							<div class="input-area">
								<form>
									<input class="email-input" type="text" placeholder="Enter your email">
									<button class="submit-btn" type="submit"><i class="ion-ios-email-outline"></i></button>
								</form>
							</div>

						</div><!-- subscribe-area -->



					</div><!-- info-area -->

				</div><!-- col-lg-4 col-md-12 -->

			</div><!-- row -->

		</div><!-- container -->
	</section><!-- section -->


@endsection

@push('js')

    <script src="{{asset('assets/frontend/js/swiper.js')}}"></script>
@endpush
