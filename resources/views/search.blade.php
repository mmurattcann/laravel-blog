@extends('layouts.frontend.app')

@section('title')
  "{{ $query}}" Aramasına İlişkin Sonuçlar
@endsection
@push('css')

  <link href="{{ asset('assets/frontend/css/category/styles.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/frontend/css/category/responsive.css') }} " rel="stylesheet">


@endpush

@section('content')
<div class="slider">
	<div class="slider display-table center-text">
		<h1 class="title display-table-cell"><b>"{{ $query}}" Aramasına İlişkin Sonuçlar</b></h1>
	</div><!-- slider -->
</div>
	<section class="blog-area section">
		<div class="container">

			<div class="row">
          @if($posts->count() > 0)
            @foreach ($posts as $post)
              <div class="col-lg-4 col-md-6">
                  <div class="card h-100">
                      <div class="single-post post-style-1">

                          <div class="blog-image"><img src="{{ url('storage/post/'.$post->image) }}" alt="{{$post->title}}"></div>

                          <a class="avatar" href="#"><img src="{{ url('storage/profile-pictures/'.$post->user->image) }}" alt="{{ $post->user->name}}"></a>

                          <div class="blog-info">

                              <h4 class="title"><a href="{{ route('post.detail',$post->slug) }}"><b>{{ $post->title }}</b></a></h4>

                              <ul class="post-footer">
                                  <li>
                                    @guest
                                       <a href="javascript:void(0);"><i class="ion-heart" onclick="toastr.warning('Favorilerinize Eklemek İçin Giriş  Yapmanız Gerekiyor...','Uyarı',{
                                           closeButton: true,
                                           progressBar: true,
                                       })">
                                            </i>{{ $post->favorite_to_users->count()}}
                                       </a>
                                    @else
                                      <a href="javascript:void(0);"><i class="ion-heart" onclick="document.getElementById('favorite-form-{{ $post->id }}').submit();"
                                           class="{{ !Auth::user()->favorite_posts()->where('post_id',$post->id)->count() == 0 ? "favorite-posts":"" }}">
                                           </i>{{ $post->favorite_to_users->count()}}
                                      </a>
                                      <form id="favorite-form-{{ $post->id }}" action="{{ route('post.favorite',$post->id) }}" method="POST" style="display:none">
                                        @csrf
                                      </form>
                                    @endguest

                                  </li>
                                  <li><i class="ion-chatbubble"></i>{{ $post->comments->count() }}</li>
                                  <li><a href="#"><i class="ion-eye"></i>{{ $post->view_count}}</a></li>
                              </ul>

                          </div><!-- blog-info -->
                      </div><!-- single-post -->
                  </div><!-- card -->
              </div><!-- col-lg-4 col-md-6 -->
            @endforeach
          @else
              <h6 class="text-center">Bu anahtar kelimeyi içeren herhangi bir gönderi bulunamadı.</h6>
            <br>
              <a href="{{ route('anasayfa') }}" class="btn btn-info">Anasayfa</a>
          @endif
			</div><!-- row -->



		</div><!-- container -->
	</section><!-- section -->


@endsection

@push('js')

@endpush
