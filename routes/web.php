<?php
/*
  ===============================================
  =        ANASAYFA YÖNLENDİRMELERİ            =
  ===============================================
*/
Route::get('/','HomeController@index')->name('anasayfa');

Route::post('subscriber','SubscriberController@store')->name('subscriber.store');
Route::get('makale/{slug}','PostController@postDetail')->name('post.detail');
Route::get('makaleler','PostController@index')->name('post.index');
Route::get('kategori/{slug}','PostController@postByCategory')->name('category.posts');
Route::get('tag/{slug}','PostController@postByTag')->name('tag.posts');
Route::get('ara','PostController@search')->name('search');
Route::get('profile/{username}','AuthorController@profile')->name('author.profile');


Auth::routes();

Route::group(['middleware' =>['auth']], function(){
    Route::post('favorite/{post}/add','FavoriteController@add')->name('post.favorite');
    Route::post('comment/{post}','CommentController@store')->name('comment.store');
});

/*
  ===============================================
  =     ADMİN YÖNETİM PANELİ YÖNLENDİRMELERİ    =
  ===============================================
*/

Route::group(['as'=>'admin.','prefix' => 'admin','namespace' => 'Admin', 'middleware' => ['auth','admin']],function(){
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('tag','TagController');
    Route::resource('category','CategoryController');
    Route::resource('post','PostController');
    Route::resource('comment','CommentController');

    Route::put('/post/{id}/approve','PostController@approval')->name('post.approve');
    Route::put('/post/{id}/removeApprove','PostController@removeApprove')->name('post.removeApprove');
    Route::get('pending/post/','PostController@pending')->name('post.pending');

    Route::get('settings','SettingsController@index')->name('settings');
    Route::put('update-settings','SettingsController@updateProfile')->name('update.profile');
    Route::put('update-password','SettingsController@updatePassword')->name('update.password');
    Route::get('/favorite','FavoriteController@index')->name('favorite.index');

    Route::get('/subscriber','SubscriberController@index')->name('subscriber.index');
    Route::delete('/subscriber/{id}', 'SubscriberController@destroy')->name('subscriber.delete');

    Route::get('authors','AuthorController@index')->name('author.index');
    Route::get('authors/{id}/','AuthorController@edit')->name('author.edit');
    Route::delete('authors/{id}','AuthorController@destroy')->name('author.destroy');

    /*
      ===============================================
      =  YAZAR YÖNETİM PANELİ YÖNLENDİRMELERİ      =
      ===============================================
    */
});
Route::group(['as' =>'author.','prefix' => 'author','namespace' => 'Author', 'middleware' => ['auth','author']],function(){
    Route::get('dashboard','DashboardController@index')->name('dashboard');

    Route::resource('post','PostController');
    Route::resource('comment','CommentController');

    Route::get('/favorite','FavoriteController@index')->name('favorite.index');

    Route::get('settings','SettingsController@index')->name('settings');
    Route::put('update-settings','SettingsController@updateProfile')->name('update.profile');
    Route::put('update-password','SettingsController@updatePassword')->name('update.password');

});
